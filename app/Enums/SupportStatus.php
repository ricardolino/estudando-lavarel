<?php

namespace App\Enums;


enum SupportStatus: string
{
    case A = "Open";
    case C = "Close";
    case P = "Peding";

    public static function fromValue(string $name): string
    {
        foreach (self::cases() as $status) {
            if ($name === $status->name) {
                return $status->value;
            }
        }

        throw new \ValueError("$staus is not a valid");
    }
}
